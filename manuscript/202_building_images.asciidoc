== Building Images

=== Agenda
// Slide 00
Hello and welcome back to this "Introduction to Docker" course. I'm Adam
Hawkins and I'll be your instructor for this lesson. In this lesson
we'll cover:

1. Building Docker Images aka the `Dockerfile`
2. Applying Docker to the software development process
3. Sharing images via the Docker registry

=== Building Images

// Slide 01
The previous lessons covered using other people's images. Now it's time
to get down and dirty building our own images. Let's return to our
original example. We used a volume mount to add our files to the
container. Instead let's use the correct approach of building an image
that _includes_ our files. This all starts with the `Dockerfile` and
`docker build`.

// CUT !

The `Dockerfile` defines everything that should go into the image, such
as files, possible volumes, ports to expose, environment variables, and
the default command. Let's build our first image now.

// CUT !

----
vim Dockerfile
----

First create a new `Dockerfile`.

// CUT !

The `Dockerfile` has it's own syntax which we'll cover in this lesson.
Each line is an instruction. The first instruction is `FROM` by
convention. `FROM` declares the base image, or where to start from.

// CUT !

Let's use `nginx` as our base image.

----
FROM nginx
----

// CUT !

Everything in base image is now available in our own image. This
includes settings for ports, volumes, and anything else.  Then we apply
our customizations on top by adding our own files or overriding previous
settings.

// CUT !

Let's add a hypotethical file to our Docker image. This file doesn't
exist yet, so we'll need to create it before we can build it. The `COPY`
command takes files specified in the first and adds to the path in the
second argument. The file is added to the directory `nginx` expects
files.

----
COPY hello.txt /usr/share/nginx/html
----

// CUT !

That's enough for this simple example. Now I'll save and quit editting
this file.

Now let's create the hypothetical file mentioned in the `Dockerfile`.
I'll just throw some text in there.

// CUT !

Now we're ready to build our first docker image. This happens via
`docker build`.

----
docker build -t ahawkins/docker-nginx-example .
----

// CUT !

There are two required values. First `-t` sets the name and tag. The
final argument sets context directory. This is the directory where files
can be copied from. We'll use `.` to represent the current directory.

// CUT !

`docker build` produces a lot of output. You'll notice that it prints
everything out in step wise fashion. There's a lot of useful debugging
information here which we'll come back to in the future.

// CUT !
Now we can start a container using our own image.

----
docker run -d \
	--name nginx \
	-p 8080:80 \
	ahawkins/docker-nginx-example
----

// CUT !

----
curl localhost:8080
----

Everything works as expected. Our docker container serves our custom
file. This simple example is about as far as we can go in step 1.
`Dockerfiles` have many features. We'll need to start building some
thing to dive deeper.

// LONG CUT for transition !!!!!
// CUT RECORDING HERE AS WELL

=== Building Applications

Let's create a simple web application to demonstrate building a Docker
image from scratch. We'll use Node.js to write a simple "Hello World"
application. Don't worry about the programming aspect. I promise you we
won't have more than 10 lines. First thing to do is create a directory
for source code.

----
mkdir -p hello-world
cd hello-world
----

// CUT !

Node works nicely for this because we can create a simple web server
without external libraries. We'll only need two files: `server.js` and a
`Dockerfile`. I've already got `server.js` ready, so I'll just paste it
in for now.

----
vim server.js
----

Now it's time for the real work, building the `Dockerfile`. We'll walk
though writing it step by step.

----
vim Dockerfile
FROM ubuntu:14.04
----

Start with `FROM` just like before. We'll use `ubuntu:14.04` because
it's easy enough to install node on. Note the `:14.04` tag. I've
specified the tag here to be explicit of _which_ ubuntu we want. It's
always better to be explicit. This will save you headaches later on.

// CUT!

Now we can go about our work installing node and adding our files.
Node.js recommends installation via a script. The script follows the
"curl/bash" pattern, so we'll need to install curl first like we did
before. We do this with the `RUN` command. Let's add commands to update
and install curl.

----
RUN apt-get update -y
RUN apt-get install -y curl
----

Note that these commands include `-y` to remove the confirmation prompt.
`Docker build` is a noninteractive process, so an interactive prompt
will not work as expected.

// CUT !

Let's check the progress so far by building the image.

----
docker build -t ahawkins/docker-intro-hello-world .
----

// CUT !

Everything is working as expected. Now let's take the next step of
installing Node.js.

----
RUN curl -sL https://deb.nodesource.com/setup_4.x | bash -
----

// CUT !

Then build again. Notice how Docker did not run the first two steps
again? This is because it's already done before. Each image is made of
multiple layers. Layers are generated by commands in the Dockerfile. So
Docker knows the layer for step 1 is already done, and the same step
two. However if we changed the commands then those steps would need to
run again and all following steps again.

// CUT !

Now add the `RUN` command to install the `node.js` package

----
RUN apt-get install -y node.js
----

Let's build again. Although it's not required to build at every step, it
may be especially useful when building an image for the first time. This
way each layer is cached and changed may be tested quickly.

// CUT !

Now lets add the server file.

----
COPY server.js /
----

Here we copy `server.js` co the root directory on the image's
filesystem. Normally you would not do this. Instead, you would use a
directory for all the source files. However this is fine enough for this
demonstration.

// CUT !

The server starts on port 8080. So now we need to add an `EXPOSE`
command.

----
EXPOSE 8080
----

// CUT !

Now finally let's set the default command via `CMD`. `CMD` takes a JSON
string array as arguments. We'll write the `CMD` to start our server.

----
CMD [ "node", "/server.js" ]`
----

// CUT !

Build again and now we're ready to take it for a spin.

----
docker run -d -p 8080:8080 --name server ahawkins/docker-intro-hello-world
----

Now `curl localhost:8080` and we get the response back from the Node
server. Not bad huh? This example gives us some decent exposure to the
Dockerfile basics. However real world applications don't only include a
single file. They have dependencies many files. Let's move onto to more
real world example.

// LONG CUT!!!!!!!!!!

==== Handling Dependencies

Most applications are not this simple. They require libraries. Let's
rework our example to use the simple Express.js web framework. We'll
need nodejs installed on our host for now to create the initial
`package.json`. First create a new directory for the source code.

----
mkdir -p hello-world-v2
cd hello-world-v2
----

// CUT !

Now we need to create the `package.json` file. I've already have Node
installed on my machine. You'll need to install it as well if you want
follow long. Run `npm init` to bootstrap a new node project.

----
npm init
----

The command will prompt you for a lot of things. Just hit enter, nothing
is required in this case.

// CUT!

Now let's add	`express` to the dependencies. 

----
npm install express --save
----

// CUT !

Next update `server.js` to use express. I've already got this
ready so I'll just paste it into my editor.

// CUT !

Now we can set about updating the `Dockerfile` accordingly. Let's use
the previous version as a base.

// CUT !

Not much is different this time around. Now we have a few files so it
makes sense to create a directory for our application.

----
RUN mkdir -p /app
----

// CUT !

We can also set the current directory _inside_ the image. 

----
WORKDIR /app
----

// CUT !

Now we need to copy `package.json`, then run `npm install` so the
dependencies are available in the image.

----
COPY package.json /app
RUN npm install
----

// CUT !

Next we just copy everything else into the image. `COPY` also takes a
directory, so we can recursively copy all files instead of adding each
file individually.

----
COPY . /app/
----

// CUT !

We do the steps in this order to keep the docker build fast. This way we
don't need to reinstall dependencies if only the application code
changes.

// CUT !

Finally we can change `CMD` to `npm start`.

// CUT !

Time to build the image.

----
docker build -t ahawkins/docker-intro-hello-world:v2 .
----

We'll use the `v2` tag to differentiate our examples.

// CUT!

Now we're ready to start the updated container.

----
docker run \
	-d \
	-p 8080:8080 \
	ahawkins/docker-intro-hello-world:v2
----

Now fire off the curl to `localhost:8080` and you'll see the response
come back.

// CUT !

The `Dockerfile` we put together contains a bunch of boiler plate. First
off we certainly don't need to install node ourself. We should use an
image with it baked in. This where the official library comes into the
picture.

// LONG CUT !!!!!

=== The Offical Library

// CUT !

The docker community maintains an official library of base images for
many common applications. We've been using them the whole time. These
images do not have a `/` in their name. So `redis`, `nginx`, `ubuntu`
are all official images. Where `foo/bar` is owned by `foo`. Image reuse
is encouraged because you're more likely to have more layers in common
with the official images than your own custom images. The official
library contains images for most programming lanauges with appropriate
version tags, databases, and even some frameworks. Odds are there is an
official image for your stack.

// CUT !

Let's work through shortening our example to use the official images.

// CUT !

We'll through editting the `Dockerfile` one step at a time.

[source,Dockerfile]
--
FROM node:4

RUN mkdir -p /app
WORKDIR /app

COPY package.json /app
RUN npm install
COPY . /app/

EXPOSE 8080

CMD [ "npm", "start" ]
--

// Cut !

We can use `node:4`, or `node:4.x`, or even `node:4.x.y`. The conention
is that `node:4` points to the latest version 4 release. `node:4.x`
points to the latest minor release. `node:4.x.y` points to a specific
patch level. Now you can see this is where things to really shine. Want to
upgrade to node 6? Just change the base image tag to `node:6`.

// CUT !

This would actually be enough, but there's more we do. Dockerfiles
usually follow the same pattern.

// CUT !

1. Install runtime and/or compile time dependencies
2. Create a directory for the application code to live
3. Copy over the application's dependency manifest file (package.json)
in thise case.
4. Install application libraries
5. Add all other files
6. Define ports, volumes, or anything else
7. Set the default command.

// CUT !

Steps one through five are generally the same, but vary depending on
language. Node.js applications expect a `package.json`. Ruby
applications expect `Gemfile`. Then a program installs the application
dependencies. 

// CUT !

The official library also includes "onbuild" images for typical use
cases. These base images have special `ONBUILD` instructions. This
allows the base image to specify commands to run _when_ the base image
is used with `FROM`. In short this allows the base image to run commands
when used.

// CUT !

Let's replace even more of our own boilerplate with the on-build image.
We need to set `WORKDIR` to `/usr/src/app` beacuse this is where onbuild
triggers copy the code too.

[source,Dockerfile]
--
FROM node:4-onbuild

WORKDIR /usr/src/app

EXPOSE 8080

CMD [ "npm", "start" ]
--

That's pretty nice. We able to go from something like 16 instructions to
just 4. The onbuild images are definitely handy when
your application follows the established pattern. Now our application
ready to go as a Docker image. It's time to share it with the world.

// CUT !

Now I'll build this docker image to highlight the "ONBUILD"
instructions. I'll reuse the same v2 tag as before.

// CUT !

// NODE: need docker registry password on clipboard before continuinging
Docker provides free public image hosting on the official registry. You
need to create a Docker Hub account before you can push images. Head
over to https://hub.docker.com to create one if you haven't already.
Once you have your usename and password you can login on the CLI.

// CUT !

I already have my account so I can login now.

----
docker login
----

// CUT !

Now you can push any tagged image. I'll push the image I just built now.

----
docker push ahawkins/docker-intro-hello-world:v2
----

You can ommit the tag and docker will push all images. This is not
recommended because you may overwrite existing tags. Tags are mutable. I
can build a new image and tag as `v2`, push it, and overwrite what was
there. You should always be explicit when pushing images.

// CUT !

Now everyone can run this hello world image. Give it a go by doing
`docker pull ahawkins/docker-intro-hello-world:v2`.

=== Conclusion

This lesson was long and jammed packed. We covered everything you need
to know to build and share your first production ready Docker image
build in accordance with community best practices.

The remainder of the course explores the docker toolchain to demonstrate
what else we can do with docker. The next lesson is on building
multi-container applications.
