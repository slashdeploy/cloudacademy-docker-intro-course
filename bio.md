Adam is backend/service engineer turned deployment and infrastructure
engineer. His passion is building rock solid services and equally
powerful deployment pipelines. He has been working with Docker for
years and leads the SRE team at Saltside. Outside of work he's a
traveller, beach bum, and trance addict.
