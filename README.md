# CloudAcademy Docker Intro Course

This repo contains source materials for the "Docker Intro" course.

## Deliverable Schedule

https://docs.google.com/a/cloudacademy.com/spreadsheets/d/17Y7Y3eXzFLQi5_raK2EGzeKks9Xt7WrbhlCIOoaypSs/edit?usp=sharing

## Directory Structure

- [outline.md][outline.md] - High level course outline
- `manuscript/` - source for each individual video
- `samples/` - reference material for my own easy access
