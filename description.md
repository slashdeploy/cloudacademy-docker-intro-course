Containers are taking the industry by storm--from production
microservices architectures to reworking development environmnets.
Docker is empowering teams to start using containers today.

My course comes from my two years of running Docker in production, and
more importantly teaching people how to use Docker. Docker changed
everything for me when I started. It enabled my team and I to create
portable and similar workflows across the different languages we used.
Docker also enabled us to deploy new services in any language. It
really felt light years ahead of where we where before. Hopefully the
course captures some of the power and helps you take a step in this
direction.

My high level go for the course is to give your first taste of Docker
and container technologies. Docker is not the first container
technology, but it's the one that hit critical mass and is changing
everything. Containerization changes how we need to think about
developing and shipping software. This course should give you enough
bearing to undrstand things you can start using today, then wider
technical topics for coming months.

We'll start off with an introduction to container technologies. It may
surprise you, but the underlying bits have actually been around for
some time. Docker is new and making them accessible to teams of all
shapes and sizes. Next we'll cover what I call the "Container
Fundamentals". This will teach you the basics of interacting with
containers, starting, stopping, networking, and docker volumes. Then
we move onto building Docker images and how to apply Docker to your
development process. The final lessons demonstrates building a
microservices application using Docker Compose and deploying it with
Docker Machine.

The course capstones with a summar and more importantly items that
could not make into the course and points for further investigation.
Hint, the next step is to look out different ways to run Docker in
production.

All sound good? Check the first video and happy shipping!
