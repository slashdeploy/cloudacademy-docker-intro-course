#!/usr/bin/env bash

set -xeuo pipefail

mkdir -p dist/audio

while read -r file; do
	ffmpeg -y -i "${file}" -vn -acodec copy "dist/audio/$(basename "${file}" ".mp4").aac" < /dev/null
done < <(find dist/videos -type f -iname '*.mp4' -print)
