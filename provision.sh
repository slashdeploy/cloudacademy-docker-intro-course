#!/usr/bin/env bash

set -euo pipefail

echo "## Purging packages"

apt-get autoremove -y

echo "## Installing Docker"

apt-get update -y

apt-get install -y \
	apt-transport-https \
	ca-certificates \
	curl

apt-key \
	adv --keyserver hkp://p80.pool.sks-keyservers.net:80 \
	--recv-keys 58118E89F3A912897C070ADBF76221572C52609D

echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" > "/etc/apt/sources.list.d/docker.list"

apt-get update -y
apt-get purge -y lxc-docker
apt-get install -y "docker-engine=1.12.0-0~trusty"

usermod -a -G docker vagrant

curl -sL https://deb.nodesource.com/setup_4.x | bash -

echo "## Installing nodejs"

apt-get install -y nodejs

echo "## Installing docker-compose"

curl -sL https://github.com/docker/compose/releases/download/1.8.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

echo "## Installing docker-machine"

curl -sL https://github.com/docker/machine/releases/download/v0.7.0/docker-machine-`uname -s`-`uname -m` > /usr/local/bin/docker-machine && \
chmod +x /usr/local/bin/docker-machine

echo "## Creating docker-nuke"

cat > /usr/local/bin/docker-nuke <<'EOF'
#!/usr/bin/env bash

set -euo pipefail

docker stop $(docker ps -aq)
docker rm -v $(docker ps -aq)

docker images -q | xargs -L1 docker rmi -f
EOF

chmod +x /usr/local/bin/docker-nuke
