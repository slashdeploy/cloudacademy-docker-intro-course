RUBY_IMAGE:=$(shell grep FROM Dockerfile | cut -f 2 -d ' ')
DOCKER_IMAGE:=tmp/image
MANUSCRIPT:=dist/manuscript.html

.DEFAULT_GOAL:=manuscript

.PHONY: check
check:
	aws --version > /dev/null
	docker --version > /dev/null

Gemfile.lock: Gemfile
	docker run --rm -it -v $(CURDIR):/data -w /data $(RUBY_IMAGE) \
		bundle package --all

$(DOCKER_IMAGE): Gemfile.lock Dockerfile
	docker build -t slashdeploy/docker-intro .
	mkdir -p $(@D)
	touch $@

$(MANUSCRIPT): manuscript/* $(DOCKER_IMAGE)
	mkdir -p $(@D)
	docker run --rm -u $(shell id -u) -v $(CURDIR):/data -w /data slashdeploy/docker-intro \
		asciidoctor -d article -D $(@D) -b html5 -o $(@F) $<

.PHONY: manuscript
manuscript: $(MANUSCRIPT)

.PHONY: clean
clean:
	rm -rf $(DOCKER_IMAGE) $(MANUSCRIPT)
